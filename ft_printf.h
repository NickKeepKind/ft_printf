/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 17:04:38 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/17 16:52:54 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stddef.h>
# include <unistd.h>
# include <stdlib.h>

// Стандатрные функции.
int	ft_putchar(char c);
int	ft_tolower(int c);
int	ft_putstr(const char *str);

// Функции для аргументов printf.
int	ft_putptr(size_t ptr);
int	ft_puthex(size_t num, unsigned int mode);
int	ft_hexovina(unsigned int num, unsigned int mode);
int	ft_unsigned_putnbr(unsigned int num);
int	ft_putnbr_len(int num);

// Функции printf.
int	ft_manage_arg(const char format, va_list varg);
int	ft_printf(const char *fmt, ...);

#endif

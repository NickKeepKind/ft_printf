/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 17:05:50 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/19 17:52:16 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "ft_printf.h"

// Функция проверяющая входящий аргумент и перенапралящая на нужную функцию.
int	ft_manage_arg(const char fmt, va_list varg)
{
	if (fmt == 'c')
		return (ft_putchar(va_arg(varg, int)));
	else if (fmt == 's')
		return (ft_putstr(va_arg(varg, char *)));
	else if (fmt == 'p')
		return (ft_putptr(va_arg(varg, size_t)));
	else if (fmt == 'x')
		return (ft_hexovina(va_arg(varg, unsigned int), 0));
	else if (fmt == 'X')
		return (ft_hexovina(va_arg(varg, size_t), 1));
	else if (fmt == 'u')
		return (ft_unsigned_putnbr(va_arg(varg, unsigned int)));
	else if (fmt == 'd' || fmt == 'i')
		return (ft_putnbr_len(va_arg(varg, int)));
	else if (fmt != '\0')
		return (ft_putchar(fmt));
	else
		return (0);
}

// Собственно сама функция Функция printf выводит в стандартный поток 
// Вывода строку отформатированную в соответствии с правилами, указанными
// В строке, на которую указывает аргумент format.. Format (в моем случае FMT)
// Это – указатель на строку c описанием формата.
int	ft_printf(const char *fmt, ...)
{
	va_list	varg;
	int		print_arg;

	va_start(varg, fmt);
	print_arg = 0;
	while (*fmt)
	{
		if (*fmt == '%' && *(fmt + 1) == '%')
		{
			write(1, ++fmt, 1);
			print_arg++;
		}
		else if (*fmt == '%')
			print_arg += ft_manage_arg(*(++fmt), varg);
		else
		{
			write(1, fmt, 1);
			print_arg++;
		}
		fmt++;
	}
	va_end(varg);
	return (print_arg);
}

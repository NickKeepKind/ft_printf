/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/07 15:18:10 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/17 15:19:06 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "ft_printf.h"

// Функция для вычисления и вывода указателя.
int	ft_putptr(size_t ptr)
{
	int	x;

	if (ptr == 0)
		return (ft_putstr("0x0"));
	x = 0;
	write(1, "0x", 2);
	x += ft_puthex(ptr, 0);
	return (x + 2);
}

// Функция преобразуеюзщая входящее число в шестнадцатиричное 
// Учитывая надобность в прописных или строчных буквах.
// С помощью просто еб***ше красивой рекурсии :)))
int	ft_puthex(size_t num, unsigned int mode)
{
	size_t	temp;
	int		hex;

	hex = 0;
	if (num != 0)
	{
		hex += ft_puthex(num / 16, mode);
		temp = num % 16;
		if (temp < 10)
			hex += ft_putchar(temp + 48);
		else
		{
			if (mode)
				hex += ft_putchar(temp + 55);
			else
				hex += ft_putchar(ft_tolower(temp + 55));
		}
	}
	return (hex);
}

// Выводит на печать (строчные и прописные) шестнадцатеричное 
// Беззнаковое число с типом по умолчанию unsigned int.
int	ft_hexovina(unsigned int num, unsigned int mode)
{
	if (num == 0)
		return (write(1, "0", 1));
	else
		return (ft_puthex(num, mode));
}

// Функция выводит число на стандартный вывод перед этим
// Переварив его в дестичное.
int	ft_unsigned_putnbr(unsigned int num)
{
	int	i;

	i = 0;
	if (num < 10)
		return (ft_putchar(num + '0'));
	i += ft_unsigned_putnbr(num / 10);
	i += ft_unsigned_putnbr((num % 10));
	return (i);
}

// Почти тоже самое что ft_unsigned_putnbr,
// Но переваривает и отрицательные числа.
int	ft_putnbr_len(int num)
{
	int	i;

	i = 0;
	if (num == -2147483648)
		return (write(1, "-2147483648", 11));
	if (num < 0)
	{
		i = ft_putchar('-');
		num *= (-1);
	}
	if (num < 10 && i != 0)
		return (ft_putchar(num + '0') + 1);
	if (num < 10)
		return (ft_putchar(num + '0'));
	i += ft_putnbr_len(num / 10);
	i += ft_putnbr_len((num % 10));
	return (i);
}

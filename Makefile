# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: kreginal <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/12/06 16:43:05 by kreginal          #+#    #+#              #
#    Updated: 2021/12/17 16:32:23 by kreginal         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

INCLUDES = ft_printf.h

CC = gcc
CFLAGS = -Wall -Werror -Wextra
RM = rm -f
LIB = ar -rcs

SRC = ft_printf.c ft_standart_utils.c ft_printf_utils.c

.c.o: ${INCLUDES}
	${CC} ${CFLAGS} -c $< -o ${<:.c=.o}

OBJ = ${SRC:.c=.o}

all: ${NAME}

$(NAME): ${OBJ}
	$(LIB) ${NAME} ${OBJ}

clean:
	${RM} ${OBJ}

fclean: clean
	${RM} ${NAME}

re: fclean all

.PHONY: all clean fclean re
